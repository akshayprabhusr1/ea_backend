from django.apps import AppConfig


class ExpenseanalysisConfig(AppConfig):
    name = 'expenseAnalysis'
