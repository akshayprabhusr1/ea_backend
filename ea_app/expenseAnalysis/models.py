from django.db import models

# Create your models here.

class Expense(models.Model):    
    description = models.TextField()
    expenseCategory = models.TextField()
    date = models.DateField()
    amount = models.IntegerField()

    def __str__(self):
        return self.description