from django.shortcuts import render

# Create your views here.

from django.shortcuts import render
from rest_framework import generics

# Create your views here.
from expenseAnalysis import models
from .serializers import ExpenseanalysisSerializer


class ListExpense(generics.ListCreateAPIView):
    queryset = models.Expense.objects.all()
    serializer_class = ExpenseanalysisSerializer


class DetailExpense(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Expense.objects.all()
    serializer_class = ExpenseanalysisSerializer