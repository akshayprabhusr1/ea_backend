from rest_framework import serializers
from expenseAnalysis import models


class ExpenseanalysisSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'description',
            'expenseCategory',
            'date',
            'amount'
        )
        model = models.Expense